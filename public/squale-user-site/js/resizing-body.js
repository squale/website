function setContentBoxHeight()
{
	var windowHeight = document.documentElement.clientHeight;
	var windowWidth = document.documentElement.clientWidth;
	var contentBox = document.getElementById("contentBox");
	var newHeight = windowHeight - 90;
	var newWidth = windowWidth - 200;
	contentBox.style.height= newHeight + "px";
	contentBox.style.width= newWidth + "px";
}